package br.com.indra.selecaojava.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe de utilitátios.
 * @author luiz
 *
 */
public class Util {

	public static List<LinhaAquivoImportacao> lerArquivo(InputStream arquivo) throws IOException{
				
		BufferedReader br = new BufferedReader(new InputStreamReader(arquivo));
		String linha = "";
		List<LinhaAquivoImportacao> linhasRetorno = new ArrayList<LinhaAquivoImportacao>();
		while ((linha = br.readLine()) != null) {

			String[] conteudo = linha.split("  ");	
			if(!conteudo[3].contains("Revenda") && conteudo.length  == 11) {
				LinhaAquivoImportacao linhaConteudo = new LinhaAquivoImportacao(conteudo[0].trim(), conteudo[1].trim(), conteudo[2], conteudo[3], conteudo[4],
						conteudo[5], conteudo[6], conteudo[7], conteudo[8], conteudo[9], conteudo[10]);
				
				linhasRetorno.add(linhaConteudo);
				System.out.println(linhaConteudo);
			} 
		}
		
		return linhasRetorno;		
	}

}
