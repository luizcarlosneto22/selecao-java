package br.com.indra.selecaojava.util;

import lombok.Data;
/**
 * Classe que recebe as informações importadas pelo arquivo.
 * @author luiz
 *
 */
@Data
public class LinhaAquivoImportacao {

	private String regiao;
	private String estado;
	private String municipio;
	private String revendedor;
	private String codigoProduto;
	private String produto;
	private String dataColeta;
	private String valorCompra;
	private String valorVenda;
	private String unidadeMedida;
	private String bandeira;
	
	
	public LinhaAquivoImportacao(String regiao, String estado, String municipio, String revendedor, String codigoProduto, String produto, String dataColeta,
			String valorCompra, String valorVenda, String unidadeMedida, String bandeira) {
		super();
		this.regiao = regiao;
		this.estado = estado;
		this.municipio = municipio;
		this.revendedor = revendedor;
		this.codigoProduto = codigoProduto;
		this.produto = produto;
		this.dataColeta = dataColeta;
		this.valorCompra = valorCompra;
		this.valorVenda = valorVenda;
		this.unidadeMedida = unidadeMedida;
		this.bandeira = bandeira;
	}

	@Override
	public String toString() {
		return "LinhaAquivoImportacao [regiao=" + regiao + ", estado=" + estado + ", municipio=" + municipio + ", revendedor=" + revendedor
				+ ", produto=" + produto + ", dataColeta=" + dataColeta + ", valorCompra=" + valorCompra
				+ ", valorVenda=" + valorVenda + ", unidadeMedida=" + unidadeMedida + ", bandeira=" + bandeira + "]";
	}
	

}

