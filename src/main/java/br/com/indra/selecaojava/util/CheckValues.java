package br.com.indra.selecaojava.util;

import br.com.indra.selecaojava.domain.enums.MensagensRetorno;
import br.com.indra.selecaojava.exception.ApplicationException;
import br.com.indra.selecaojava.web.api.dto.request.RevendedorRequest;

/**
 * Classe que realiza a checagem de valores e atributos informados em requisições GET.
 * @author luiz
 *
 */
public class CheckValues {

	/**
	 * Verificar se o valor está em branco ou nulo.
	 * @param valor
	 * @throws ApplicationException
	 */
	public static void verificarValoresRequisicao(String valor) throws ApplicationException {
		if(valor == null || "".equals(valor)) {
			throw new ApplicationException(MensagensRetorno.CAMPO_NAO_INFORMADO.getCodigo(),
					MensagensRetorno.CAMPO_NAO_INFORMADO.getMensagem());
		}
	}
	
	/**
	 * Verifica os valores de revendedor antes de ser salvo.
	 * @param revendedor
	 * @throws ApplicationException
	 */
	public static void verificarValoresParaSalvarUmaRevenda(RevendedorRequest revendedor) throws ApplicationException {
		boolean checkValores = true;
		if(revendedor.getRegiao() == null || "".equals(revendedor.getRegiao())) {
			checkValores =false;
		}
		if(revendedor.getEstado() == null || "".equals(revendedor.getEstado())) {
			checkValores =false;
		}
		if(revendedor.getRegiao() == null || "".equals(revendedor.getRegiao())) {
			checkValores =false;
		}
		if(revendedor.getNomeMunicipio() == null || "".equals(revendedor.getNomeMunicipio())) {
			checkValores =false;
		}
		if(revendedor.getNomeRevendedor() == null || "".equals(revendedor.getNomeRevendedor())) {
			checkValores = false;
		}
		
		if(!checkValores) {
			throw new ApplicationException(MensagensRetorno.CAMPO_NAO_INFORMADO.getCodigo(),
					MensagensRetorno.CAMPO_NAO_INFORMADO.getMensagem());
		}
		
	}
}
