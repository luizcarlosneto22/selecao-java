package br.com.indra.selecaojava.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
* Classe de conversão de datas.
* @author luiz
*
*/

public class DateUtil {

	public static Date converteData(String data) {
		try {
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
			Date dataConvertida = null;
			if (data != null && !data.isEmpty()) {
				try {
					dataConvertida = formato.parse(data);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			return dataConvertida;
		} catch (Exception e) {
		}
		return new Date();
	}
	
}