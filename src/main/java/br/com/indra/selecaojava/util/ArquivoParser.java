package br.com.indra.selecaojava.util;

import java.math.BigDecimal;

import br.com.indra.selecaojava.domain.enums.Operacao;
import br.com.indra.selecaojava.domain.enums.TipoCombustivel;
import br.com.indra.selecaojava.domain.enums.UnidadeDeMedida;
import br.com.indra.selecaojava.domain.historicoprecocombustivel.HistoricoPrecoCombustivel;
import br.com.indra.selecaojava.domain.revendedor.Revendedor;

/**
 * Classe que realizar o parser dos dados importados pelo arquivo.
 * @author luiz
 *
 */
public class ArquivoParser {

	
	/**
	 * 
	 * Metodo responsavel por convertar uma linha lido do arquivo para um objeto revendedor.
	 * @param linhaAquivoImportacao
	 * @return revendedor montado;
	 */
	public static Revendedor converteLinhaToRevendedo(LinhaAquivoImportacao linhaAquivoImportacao) {

		Revendedor revendedor = new Revendedor(linhaAquivoImportacao.getRegiao(), linhaAquivoImportacao.getEstado(), linhaAquivoImportacao.getMunicipio(),
				linhaAquivoImportacao.getRevendedor());
		return revendedor;
	}

	/**
	 * 
	 * Metodo responsavel por converter uma linha em histórico de preço de combustível
	 * 
	 * @param linhaAquivoImportacao
	 * @param revendedor
	 * @param tipo       de oprecao (compra/venda)
	 * @return Historico de preco
	 */
	public static HistoricoPrecoCombustivel converteLinhaToHistorico(LinhaAquivoImportacao linhaAquivoImportacao) {
		try {
			return new HistoricoPrecoCombustivel(linhaAquivoImportacao.getCodigoProduto(), 
					TipoCombustivel.recuperaValorPorDescricao(linhaAquivoImportacao.getProduto()),
					DateUtil.converteData(linhaAquivoImportacao.getDataColeta()),
					new BigDecimal(formatarCampoNumericoComVirgula(validarCampoNumetoEmBranco(linhaAquivoImportacao.getValorCompra()))),
					new BigDecimal(formatarCampoNumericoComVirgula(validarCampoNumetoEmBranco(linhaAquivoImportacao.getValorVenda()))),
					UnidadeDeMedida.LITRO,
					verificarTipoOperacao(linhaAquivoImportacao.getValorCompra()),
					linhaAquivoImportacao.getBandeira());
		} catch (NullPointerException | NumberFormatException e) {
			
		}
		return null;
	}
	
	
	public static String validarCampoNumetoEmBranco(String valor) {
		if (valor == null || "".equals(valor)) {
			return "0";
		}
		return valor;
	}
	
	public static String formatarCampoNumericoComVirgula(String valor) {
		if (valor.contains(",")) {
			return valor.replace(",", ".");
		}
		return valor;
	}
	
	public static Operacao verificarTipoOperacao(String valorCompra) {
		return "".equals(valorCompra) ? Operacao.VENDA : Operacao.COMPRA;
	}

}
