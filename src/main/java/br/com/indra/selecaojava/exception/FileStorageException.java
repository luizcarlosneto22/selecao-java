package br.com.indra.selecaojava.exception;

/**
 * Exception lançada caso ocorra algum error na importação do arquivo.
 * @author luiz
 *
 */
public class FileStorageException extends RuntimeException {
    public FileStorageException(String message) {
        super(message);
    }

    public FileStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}