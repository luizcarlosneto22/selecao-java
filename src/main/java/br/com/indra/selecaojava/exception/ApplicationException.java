package br.com.indra.selecaojava.exception;

import lombok.Data;

/**
 * Exception que é lançada em caso de erros operacionais da aplicação.
 * @author luiz
 *
 */
@Data
public class ApplicationException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String errorCode;
	private String errorMessage;
	
	public ApplicationException(String mensagem, Throwable e) {
		super(mensagem, e);
	}
	
	public ApplicationException (String errorCode, String errorMessage) {
		setErrorCode(errorCode);
		setErrorMessage(errorMessage);
	}
	
	public ApplicationException (String errorMessage) {
		setErrorCode("");
		setErrorMessage(errorMessage);
	}
}
