package br.com.indra.selecaojava.web.api.dto.response;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * Classe de resposta para requisições relacionadas ao revendedor.
 * @author luiz
 *
 */
@Data
public class RevendedorResponse extends DefaultResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String regiao;
	private String estado;
	private String nomeMunicipio;
	private String nomeRevendedor;
	private List<HistoricoPrecoCombustivelResponse> lista;
}
