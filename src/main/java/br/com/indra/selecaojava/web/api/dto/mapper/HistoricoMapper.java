package br.com.indra.selecaojava.web.api.dto.mapper;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.indra.selecaojava.domain.enums.Operacao;
import br.com.indra.selecaojava.domain.enums.UnidadeDeMedida;
import br.com.indra.selecaojava.domain.historicoprecocombustivel.HistoricoPrecoCombustivel;
import br.com.indra.selecaojava.util.DateUtil;
import br.com.indra.selecaojava.web.api.dto.request.HistoricoPrecoCombustivelRequest;
import br.com.indra.selecaojava.web.api.dto.response.HistoricoPrecoCombustivelResponse;

/**
 * Classe que realiza o parser dos objetos informados nas requisições para histórico preço combustível.
 * @author luiz
 *
 */
public class HistoricoMapper {

	/**
	 * Realiza um aprser da classe HistoricoPrecoCombustivelRequest para a HistoricoPrecoCombustivel.
	 * @param historicoRequest
	 * @return
	 * @throws ParseException
	 */
	public static HistoricoPrecoCombustivel parserHistoricoRequestForHistorico(HistoricoPrecoCombustivelRequest historicoRequest) throws ParseException {
		HistoricoPrecoCombustivel historico = new HistoricoPrecoCombustivel();
		historico.setCodigoProduto(historicoRequest.getCodigoProduto());
		historico.setDataColeta(DateUtil.converteData(historicoRequest.getDataColeta()));
		historico.setValorCompra(new BigDecimal(historicoRequest.getValorCompra()));
		historico.setValorVenda(new BigDecimal(historicoRequest.getValorVenda()));
		historico.setUnidadeMedida(UnidadeDeMedida.LITRO);
		if ("".equals(historicoRequest.getValorVenda())) {
			historico.setTipoOperacao(Operacao.VENDA);
		} else {
			historico.setTipoOperacao(Operacao.COMPRA);
		}
		historico.setNomeBandeira(historicoRequest.getNomeBandeira());
		return historico;
	}
	
	/**
	 * Realiza um aprser da classe HistoricoPrecoCombustivel para a HistoricoPrecoCombustivelResponse.
	 * @param listaHistorico
	 * @return
	 */
	public static List<HistoricoPrecoCombustivelResponse> parserHistoricoForHistoricoResponse(List<HistoricoPrecoCombustivel> listaHistorico) {
		List<HistoricoPrecoCombustivelResponse> listHistoricoPrecoCombustivelResponse = new ArrayList<>();
		listaHistorico.forEach(historicoPrecoCombustivel->{
			HistoricoPrecoCombustivelResponse bean = new HistoricoPrecoCombustivelResponse();
			bean.setCodigoProduto(historicoPrecoCombustivel.getCodigoProduto());
			bean.setProduto(historicoPrecoCombustivel.getProduto());
			bean.setDataColeta(historicoPrecoCombustivel.getDataColeta());
			bean.setValorCompra(historicoPrecoCombustivel.getValorCompra());
			bean.setValorVenda(historicoPrecoCombustivel.getValorVenda());
			bean.setUnidadeMedida(historicoPrecoCombustivel.getUnidadeMedida());
			bean.setTipoOperacao(historicoPrecoCombustivel.getTipoOperacao());
			bean.setNomeBandeira(historicoPrecoCombustivel.getNomeBandeira());
			listHistoricoPrecoCombustivelResponse.add(bean);
			bean = new HistoricoPrecoCombustivelResponse();
		});
		return listHistoricoPrecoCombustivelResponse;
	}
}
