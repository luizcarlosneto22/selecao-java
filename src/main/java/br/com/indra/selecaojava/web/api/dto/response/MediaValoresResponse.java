package br.com.indra.selecaojava.web.api.dto.response;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

/**
 * Classe de resposta para requisições relacionadas a média de preço do combustível.
 * @author luiz
 *
 */

@Data
public class MediaValoresResponse extends DefaultResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private	BigDecimal media;

	public MediaValoresResponse() {
		super();
	}

	public MediaValoresResponse(String codigo, String mensagem) {
		super(codigo, mensagem);
	}
	
}
