package br.com.indra.selecaojava.web.api.dto.response;

import lombok.Data;

/**
 * Classe de resposta para requisições relacionadas ao usuário.
 * @author luiz
 *
 */
@Data
public class UsuarioResponse extends DefaultResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private String email;
	private String login;
	private String senha;
	
	public UsuarioResponse() {}

	public UsuarioResponse(String codigo, String mensagem) {
		super(codigo, mensagem);
	}
	
	
}
