package br.com.indra.selecaojava.web.api.dto.request;

import lombok.Data;

/**
 * Classe padrão para requisições relacionadas a classe usuário.
 * @author luiz
 *
 */
@Data
public class UsuarioRequest {

	private String nome;
	private String email;
	private String login;
	private String senha;
}
