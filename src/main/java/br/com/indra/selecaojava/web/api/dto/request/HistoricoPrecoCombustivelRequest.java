package br.com.indra.selecaojava.web.api.dto.request;


import br.com.indra.selecaojava.domain.enums.TipoCombustivel;
import lombok.Data;

/**
 * Classe padrão para requisições relacionadas a classe histórico preço combustível.
 * @author luiz
 *
 */
@Data
public class HistoricoPrecoCombustivelRequest {
	private String codigoProduto;
	private TipoCombustivel produto;
	private String dataColeta;
	private String valorCompra;
	private String valorVenda;
	private String nomeBandeira;
	private String codigoHistorico;

}
