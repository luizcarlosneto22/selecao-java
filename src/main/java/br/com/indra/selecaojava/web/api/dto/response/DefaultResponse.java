package br.com.indra.selecaojava.web.api.dto.response;

import java.io.Serializable;

import lombok.Data;

/**
 * Classe padrão para resposta das requisições.
 * @author luiz
 *
 */
@Data
public class DefaultResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private String codigo;
	private String mensagem;
	public DefaultResponse(String codigo, String mensagem) {
		super();
		this.codigo = codigo;
		this.mensagem = mensagem;
	}
	public DefaultResponse() {}
	
}
