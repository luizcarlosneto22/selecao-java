package br.com.indra.selecaojava.web.api.dto.mapper;

import br.com.indra.selecaojava.domain.enums.MensagensRetorno;
import br.com.indra.selecaojava.domain.usuario.Usuario;
import br.com.indra.selecaojava.web.api.dto.request.UsuarioRequest;
import br.com.indra.selecaojava.web.api.dto.response.UsuarioResponse;

/**
 * Classe que realizar o parser dos objetos informados na em requisições para usuário.
 * @author luiz
 *
 */
public class UsuarioMapper {

	/**
	 * Método qu realiza o parser da classe UsuarioRequest para Usuario.
	 * @param usuarioRequest
	 * @return
	 */
	public static  Usuario parserUsuarioRequestForUsuario(UsuarioRequest usuarioRequest) {
		Usuario usuario = new Usuario();
		usuario.setNome(usuarioRequest.getNome());
		usuario.setEmail(usuarioRequest.getEmail());
		usuario.setLogin(usuarioRequest.getLogin());
		usuario.setSenha(usuarioRequest.getSenha());
		return usuario;
	}
	
	/**
	 * Método qu realiza o parser da classe Usuario para UsuarioResponse.
	 * @param usuarioRequest
	 * @return
	 */
	public static UsuarioResponse parserUsuarioForUsuarioResponse(Usuario usuarioRequest) {
		UsuarioResponse usuario = new UsuarioResponse();
		usuario.setNome(usuarioRequest.getNome());
		usuario.setEmail(usuarioRequest.getEmail());
		usuario.setLogin(usuarioRequest.getLogin());
		usuario.setSenha(usuarioRequest.getSenha());
		usuario.setCodigo(MensagensRetorno.RETORNO_SUCESSO.getCodigo());
		usuario.setMensagem(MensagensRetorno.RETORNO_SUCESSO.getMensagem());
		return usuario;
	}
}
