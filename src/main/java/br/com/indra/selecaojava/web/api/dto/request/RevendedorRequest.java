package br.com.indra.selecaojava.web.api.dto.request;

import lombok.Data;

/**
 * Classe padrão para requisições relacionadas a classe revendedor.
 * @author luiz
 *
 */
@Data
public class RevendedorRequest {
	private String regiao;
	private String estado;
	private String nomeMunicipio;
	private String nomeRevendedor;
	private String codigoRevendedor;
}
