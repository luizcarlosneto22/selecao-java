package br.com.indra.selecaojava.web.api.dto.response;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import br.com.indra.selecaojava.domain.enums.Operacao;
import br.com.indra.selecaojava.domain.enums.TipoCombustivel;
import br.com.indra.selecaojava.domain.enums.UnidadeDeMedida;
import lombok.Data;

/**
 * Classe que retorna a responsta de consultas relacionadas ao histórico de preço de combustível.
 * @author luiz
 *
 */
@Data
public class HistoricoPrecoCombustivelResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String codigoProduto;
	private TipoCombustivel produto;
	private Date dataColeta;
	private BigDecimal valorCompra;
	private BigDecimal valorVenda;
	private UnidadeDeMedida unidadeMedida;
	private Operacao tipoOperacao;
	private String nomeBandeira;
	
}
