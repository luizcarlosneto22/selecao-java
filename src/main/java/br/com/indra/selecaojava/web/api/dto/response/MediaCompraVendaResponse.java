package br.com.indra.selecaojava.web.api.dto.response;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

/**
 * Classe de resposta para requisições relacionadas a média de compra e venda.
 * @author luiz
 *
 */
@Data
public class MediaCompraVendaResponse extends DefaultResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal mediaCompra;
	private BigDecimal mediaVenda;
	public MediaCompraVendaResponse(String codigo, String mensagem, BigDecimal mediaCompra, BigDecimal mediaVenda) {
		super(codigo, mensagem);
		this.mediaCompra = mediaCompra;
		this.mediaVenda = mediaVenda;
	}
	
	
}

