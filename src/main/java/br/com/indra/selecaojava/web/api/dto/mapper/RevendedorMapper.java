package br.com.indra.selecaojava.web.api.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import br.com.indra.selecaojava.domain.historicoprecocombustivel.HistoricoPrecoCombustivel;
import br.com.indra.selecaojava.domain.revendedor.Revendedor;
import br.com.indra.selecaojava.web.api.dto.request.RevendedorRequest;
import br.com.indra.selecaojava.web.api.dto.response.HistoricoPrecoCombustivelResponse;
import br.com.indra.selecaojava.web.api.dto.response.RevendedorResponse;

/**
 * Classe que realizar o parser dos objetos informados na em requisições para revendedor.
 * @author luiz
 *
 */
public class RevendedorMapper {

	/**
	 * Método para realizar um parser da classe RevendedorRequest para a classe Revendedor.
	 * @param historicoRequest
	 * @return
	 */
	
	public static Revendedor parserRevendedorRequestForRevendedor(RevendedorRequest historicoRequest) {
		Revendedor revendedor = new Revendedor();
		revendedor.setRegiao(historicoRequest.getRegiao());
		revendedor.setEstado(historicoRequest.getEstado());
		revendedor.setNomeMunicipio(historicoRequest.getNomeMunicipio());
		revendedor.setNomeRevendedor(historicoRequest.getNomeRevendedor());
		revendedor.setCodigoRevendedor(historicoRequest.getCodigoRevendedor());
		return revendedor;
	}
	
	/**
	 * Método para realizar o parser da classe Revendedor para RevendedorResponse.
	 * @param listaRevendedor
	 * @return
	 */
	public static List<RevendedorResponse> parserRevendedorForRevendedorResponse(List<Revendedor> listaRevendedor) {
		List<RevendedorResponse> listRevendedor = new ArrayList<>();
		
		listaRevendedor.forEach(revendedor->{			
			List<HistoricoPrecoCombustivelResponse> listHistoricoPrecoCombustivelResponse = new ArrayList<>();
			RevendedorResponse revendedorResponse = new RevendedorResponse();
			revendedorResponse.setEstado(revendedor.getEstado());
			revendedorResponse.setNomeMunicipio(revendedor.getNomeMunicipio());
			revendedorResponse.setRegiao(revendedor.getRegiao());
			revendedor.setNomeRevendedor(revendedor.getNomeRevendedor());
			for (HistoricoPrecoCombustivel historicoPrecoCombustivel : revendedor.getHistoricoPreco()) {
				HistoricoPrecoCombustivelResponse bean = new HistoricoPrecoCombustivelResponse();
				bean.setCodigoProduto(historicoPrecoCombustivel.getCodigoProduto());
				bean.setProduto(historicoPrecoCombustivel.getProduto());
				bean.setDataColeta(historicoPrecoCombustivel.getDataColeta());
				bean.setValorCompra(historicoPrecoCombustivel.getValorCompra());
				bean.setValorVenda(historicoPrecoCombustivel.getValorVenda());
				bean.setUnidadeMedida(historicoPrecoCombustivel.getUnidadeMedida());
				bean.setTipoOperacao(historicoPrecoCombustivel.getTipoOperacao());
				bean.setNomeBandeira(historicoPrecoCombustivel.getNomeBandeira());
				listHistoricoPrecoCombustivelResponse.add(bean);
				bean = new HistoricoPrecoCombustivelResponse();
			}
			revendedorResponse.setLista(listHistoricoPrecoCombustivelResponse);
			listRevendedor.add(revendedorResponse);
			listHistoricoPrecoCombustivelResponse = new ArrayList<>();
		});
		return listRevendedor;
	}
}
