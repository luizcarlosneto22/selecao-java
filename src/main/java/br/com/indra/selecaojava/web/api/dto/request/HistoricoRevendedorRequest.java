package br.com.indra.selecaojava.web.api.dto.request;

import lombok.Data;

/**
 * Classe utilizada no cadastro de um novo histórico de preço  de combustível.
 * @author luiz
 *
 */
@Data
public class HistoricoRevendedorRequest {

	private HistoricoPrecoCombustivelRequest historico;
	private RevendedorRequest revendedor;
}
