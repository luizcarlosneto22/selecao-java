package br.com.indra.selecaojava.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * Classe que carrega as informações dos path relacionados ao upload dos arquivos. 
 * @author luiz
 *
 */
@ConfigurationProperties
@Data
public class FileStorageProperties {
	
	@Value("${file:upload-dir}")
	private String uploadDir;
}
