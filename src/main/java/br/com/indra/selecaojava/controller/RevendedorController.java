package br.com.indra.selecaojava.controller;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.indra.selecaojava.domain.enums.MensagensRetorno;
import br.com.indra.selecaojava.domain.revendedor.Revendedor;
import br.com.indra.selecaojava.service.revendedor.RevendedorService;
import br.com.indra.selecaojava.web.api.dto.mapper.RevendedorMapper;
import br.com.indra.selecaojava.web.api.dto.response.MediaValoresResponse;
import br.com.indra.selecaojava.web.api.dto.response.RevendedorResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
/**
 * Classe que contém todos os endpoint referentes ao revendedor.
 * @author luiz
 *
 */
@RestController
@RequestMapping("/revendedor")
public class RevendedorController {
	
	@Autowired
	private RevendedorService revendedorService;
	
	@ApiOperation(
			value="Retorna todas as informações importadas por região.", 
			response=ResponseEntity.class, 
			notes="Essa operação busca retorna todas as informações importadas por região.")
	@ApiResponses(value= {
			@ApiResponse(
					code=200, 
					message="Retorna um ResponseEntity com uma mensagem de sucesso",
					response=ResponseEntity.class
					),
			@ApiResponse(
					code=500, 
					message="Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception",
					response=ResponseEntity.class
					)
	})
	@RequestMapping(method = RequestMethod.GET, path = "/retornartodos/regiao")
	public ResponseEntity<List<RevendedorResponse>> daodsAgrupadosPorRegiao() {
		List<Revendedor> lista = revendedorService.buscarDadosAgrupadosPorRegiao();
		List<RevendedorResponse> response = RevendedorMapper.parserRevendedorForRevendedorResponse(lista);
		return new ResponseEntity<>(response, HttpStatus.OK);
		
	}
	
	@ApiOperation(
			value="Retorna todas as informações agrupadas por revendedor", 
			response=ResponseEntity.class, 
			notes="Essa operação busca retornar todas as informações agrupadas por revendedor.")
	@ApiResponses(value= {
			@ApiResponse(
					code=200, 
					message="Retorna um ResponseEntity com uma mensagem de sucesso",
					response=ResponseEntity.class
					),
			@ApiResponse(
					code=500, 
					message="Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception",
					response=ResponseEntity.class
					)
	})
	@RequestMapping(method = RequestMethod.GET, path = "/retornartodos/revendedor")
	public ResponseEntity<List<RevendedorResponse>> dadosAgrupadoPorRevendedor() {
		List<Revendedor> lista = revendedorService.buscarDadosAgrupadosPorRevendedor();
		List<RevendedorResponse> response = RevendedorMapper.parserRevendedorForRevendedorResponse(lista);
		return new ResponseEntity<>(response, HttpStatus.OK);
		
	}
	
	@ApiOperation(
			value="Retorna a média de preço de combustível com base no nome do município", 
			response=ResponseEntity.class, 
			notes="Essa operação busca a média do preço do combustivel com base no  municipio.")
	@ApiResponses(value= {
			@ApiResponse(
					code=200, 
					message="Retorna um ResponseEntity com uma mensagem de sucesso",
					response=ResponseEntity.class
					),
			@ApiResponse(
					code=500, 
					message="Caso tenhamos algum erro vamos retornar um ResponseModel com a Exception",
					response=ResponseEntity.class
					)
	})
	@RequestMapping(method = RequestMethod.GET, path = "/mediaprecocombustivel/municipio")
	public ResponseEntity<?> mediaPeecoCombustivel(@RequestParam("nomeMunicipio") String nomeMunicipio) {
		MediaValoresResponse response = null;
		BigDecimal media = revendedorService.buscarMediaDePrecoPorMunicipio(nomeMunicipio);
		response = new  MediaValoresResponse(MensagensRetorno.RETORNO_SUCESSO.getCodigo(), MensagensRetorno.RETORNO_SUCESSO.getMensagem());
		response.setMedia(media);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
