package br.com.indra.selecaojava.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.indra.selecaojava.domain.enums.MensagensRetorno;
import br.com.indra.selecaojava.domain.historicoprecocombustivel.HistoricoPrecoCombustivel;
import br.com.indra.selecaojava.domain.revendedor.Revendedor;
import br.com.indra.selecaojava.exception.ApplicationException;
import br.com.indra.selecaojava.service.historicoprecocombustivel.HistoricoPrecoCombustivelService;
import br.com.indra.selecaojava.util.CheckValues;
import br.com.indra.selecaojava.web.api.dto.mapper.HistoricoMapper;
import br.com.indra.selecaojava.web.api.dto.mapper.RevendedorMapper;
import br.com.indra.selecaojava.web.api.dto.request.HistoricoRevendedorRequest;
import br.com.indra.selecaojava.web.api.dto.response.DefaultResponse;
import br.com.indra.selecaojava.web.api.dto.response.HistoricoPrecoCombustivelResponse;
import br.com.indra.selecaojava.web.api.dto.response.MediaCompraVendaResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Classe que contém todos os endpoint relacionados ao Histórico de preço de combustível. 
 * @author luiz
 *
 */
@RestController
@RequestMapping("/historico")
public class HistoricoPrecoCombustivelController {

	
	@Autowired
	private HistoricoPrecoCombustivelService historicoPrecoCombustivelService;
	
	@ApiOperation(
			value="Cadastrar um novo histórico de preço de combustivel", 
			response=ResponseEntity.class, 
			notes="Essa operação registra um novo historico de preco de combustível na base.")
	@ApiResponses(value= {
			@ApiResponse(
					code=200, 
					message="Retorna um ResponseEntity com uma mensagem de sucesso",
					response=ResponseEntity.class
					),
			@ApiResponse(
					code=500, 
					message="Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception",
					response=ResponseEntity.class
					)
	})
	@RequestMapping(method = RequestMethod.POST, path = "/salvar")
	public ResponseEntity<?> cadastrarUsuario(@RequestBody HistoricoRevendedorRequest historicoRequest) throws ParseException {
		DefaultResponse response = null;
		try {
			CheckValues.verificarValoresRequisicao(historicoRequest.getRevendedor().getCodigoRevendedor());
			HistoricoPrecoCombustivel historico = HistoricoMapper.parserHistoricoRequestForHistorico(historicoRequest.getHistorico());
			Revendedor revendedor = RevendedorMapper.parserRevendedorRequestForRevendedor(historicoRequest.getRevendedor());
			historicoPrecoCombustivelService.salvar(historico, revendedor);
			response = new  DefaultResponse(MensagensRetorno.RETORNO_SUCESSO.getCodigo(), MensagensRetorno.RETORNO_SUCESSO.getMensagem());
		} catch (ApplicationException e) {
			response = new  DefaultResponse(MensagensRetorno.ERRO_INTERNO.getCodigo(), MensagensRetorno.ERRO_INTERNO.getMensagem());
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
		
	}
	
	@ApiOperation(
			value="Retorna o valor médio de compra e venda por município", 
			response=ResponseEntity.class, 
			notes="Essa operação busca a média do preço de compra e venda do combustivel com base no municipio.")
	@ApiResponses(value= {
			@ApiResponse(
					code=200, 
					message="Retorna um ResponseEntity com uma mensagem de sucesso",
					response=ResponseEntity.class
					),
			@ApiResponse(
					code=500, 
					message="Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception",
					response=ResponseEntity.class
					)
	})
	@RequestMapping(method = RequestMethod.GET, path = "/media/compravenda/municipio")
	public ResponseEntity<MediaCompraVendaResponse> dadosAgrupadoPorMunicipio(@RequestParam("nomeMunicipio") String nomeMunicipio) {
		MediaCompraVendaResponse response = null;
		BigDecimal mediaValorCompra = historicoPrecoCombustivelService.mediaCompraCompustivelPorMunicipio(nomeMunicipio);
		BigDecimal mediaValorVenda = historicoPrecoCombustivelService.mediaVendaCompustivelPorMunicipio(nomeMunicipio);
		response = new  MediaCompraVendaResponse(MensagensRetorno.RETORNO_SUCESSO.getCodigo(), MensagensRetorno.RETORNO_SUCESSO.getMensagem(), mediaValorCompra, mediaValorVenda);
		return new ResponseEntity<MediaCompraVendaResponse>(response, HttpStatus.OK);
		
	}
	
	@ApiOperation(
			value="Retorna os dados agrupados pela bandeira)", 
			response=ResponseEntity.class, 
			notes="Essa operação retorna todos os dados agrupados bandeira.")
	@ApiResponses(value= {
			@ApiResponse(
					code=200, 
					message="Retorna um ResponseEntity com uma mensagem de sucesso",
					response=ResponseEntity.class
					),
			@ApiResponse(
					code=500, 
					message="Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception",
					response=ResponseEntity.class
					)
	})
	@RequestMapping(method = RequestMethod.GET, path = "/media/compravenda/bandeira")
	public ResponseEntity<MediaCompraVendaResponse> dadosAgrupadoPorPorBandeira(@RequestParam("nomeBandeira") String nomeBandeira) {
		MediaCompraVendaResponse response = null;
		BigDecimal mediaValorCompra = historicoPrecoCombustivelService.mediaCompraCompustivelPorBandeira(nomeBandeira);
		BigDecimal mediaValorVenda = historicoPrecoCombustivelService.mediaVendaCompustivelPorBandeira(nomeBandeira);
		response = new  MediaCompraVendaResponse(MensagensRetorno.RETORNO_SUCESSO.getCodigo(), MensagensRetorno.RETORNO_SUCESSO.getMensagem(), mediaValorCompra, mediaValorVenda);
		return new ResponseEntity<MediaCompraVendaResponse>(response, HttpStatus.OK);
	}
	@ApiOperation(
			value="retorna todos os dados agrupados por datar de coleta", 
			response=ResponseEntity.class, 
			notes="Essa operação retorna os todos os dados agrupados por data de coleta.")
	@ApiResponses(value= {
			@ApiResponse(
					code=200, 
					message="Retorna um ResponseEntity com uma mensagem de sucesso",
					response=ResponseEntity.class
					),
			@ApiResponse(
					code=500, 
					message="Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception",
					response=ResponseEntity.class
					)
	})
	@RequestMapping(method = RequestMethod.GET, path = "/retornartodos/datacoleta")
	public ResponseEntity<List<HistoricoPrecoCombustivelResponse>> dadosAgrupadoPordataColeta() {
		List<HistoricoPrecoCombustivel> lista = historicoPrecoCombustivelService.buscarDadosAgrupadosPorDataColeta();
		List<HistoricoPrecoCombustivelResponse> response = HistoricoMapper.parserHistoricoForHistoricoResponse(lista);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
