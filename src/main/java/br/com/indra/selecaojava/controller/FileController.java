package br.com.indra.selecaojava.controller;

import java.io.IOException;

import javax.servlet.annotation.MultipartConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.indra.selecaojava.domain.enums.MensagensRetorno;
import br.com.indra.selecaojava.service.file.FileStorageService;
import br.com.indra.selecaojava.web.api.dto.response.DefaultResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Classe que contém todos os endpoint relacionados a importação do arquivo. 
 * @author luiz
 *
 */
@RestController
@MultipartConfig(maxFileSize = 10737418240L, maxRequestSize = 10737418240L, fileSizeThreshold = 52428800)
public class FileController {

	    @Autowired
	    private FileStorageService fileStorageService;
	    
	    
		@ApiOperation(
				value="Importarção do arquivo csv", 
				response=ResponseEntity.class, 
				notes="Essa operação realiza a importação do arquivo csv, que contém os dados a serem trabalhados.")
		@ApiResponses(value= {
				@ApiResponse(
						code=200, 
						message="Retorna um ResponseEntity com uma mensagem de sucesso",
						response=ResponseEntity.class
						),
				@ApiResponse(
						code=500, 
						message="Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception",
						response=ResponseEntity.class
						)
		})
	    @PostMapping("/importar-arquivo")
	    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
	        DefaultResponse response = null;
	        try {
				fileStorageService.importarArquivoCadastrando(file);
				response = new  DefaultResponse(MensagensRetorno.RETORNO_SUCESSO.getCodigo(), MensagensRetorno.RETORNO_SUCESSO.getMensagem());
			} catch (IOException e) {
				e.printStackTrace();
			}
	       return new ResponseEntity<>(response, HttpStatus.OK);
	    }

	}
