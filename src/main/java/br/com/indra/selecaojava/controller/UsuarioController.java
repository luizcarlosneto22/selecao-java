package br.com.indra.selecaojava.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.indra.selecaojava.domain.enums.MensagensRetorno;
import br.com.indra.selecaojava.domain.usuario.Usuario;
import br.com.indra.selecaojava.exception.ApplicationException;
import br.com.indra.selecaojava.service.usuario.UsuarioService;
import br.com.indra.selecaojava.web.api.dto.mapper.UsuarioMapper;
import br.com.indra.selecaojava.web.api.dto.request.UsuarioRequest;
import br.com.indra.selecaojava.web.api.dto.response.DefaultResponse;
import br.com.indra.selecaojava.web.api.dto.response.UsuarioResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Classe que contém todos os endpoint referentes ao usuário.
 * @author luiz
 *
 */

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	
	
	@ApiOperation(
			value="Cadastrar um novo usuário", 
			response=ResponseEntity.class, 
			notes="Essa operação salva um novo registro com as informações de um usuario.")
	@ApiResponses(value= {
			@ApiResponse(
					code=200, 
					message="Retorna um ResponseEntity com uma mensagem de sucesso",
					response=ResponseEntity.class
					),
			@ApiResponse(
					code=500, 
					message="Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception",
					response=ResponseEntity.class
					)
	})
	@RequestMapping(method = RequestMethod.POST, path = "/salvar")
	public ResponseEntity<?> cadastrarUsuario(@RequestBody UsuarioRequest usuarioDTO) {
		DefaultResponse response = null;
		Usuario usuario = UsuarioMapper.parserUsuarioRequestForUsuario(usuarioDTO);
		usuarioService.salvar(usuario);
		response = new  DefaultResponse(MensagensRetorno.RETORNO_SUCESSO.getCodigo(), MensagensRetorno.RETORNO_SUCESSO.getMensagem());
		return new ResponseEntity<>(response, HttpStatus.OK);
		
	}
	
	@ApiOperation(
			value="Deletar um novo usuário", 
			response=ResponseEntity.class, 
			notes="Essa operação realiza remoção de um usuário.")
	@ApiResponses(value= {
			@ApiResponse(
					code=200, 
					message="Retorna um ResponseEntity com uma mensagem de sucesso",
					response=ResponseEntity.class
					),
			@ApiResponse(
					code=500, 
					message="Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception",
					response=ResponseEntity.class
					)
	})
	@RequestMapping(method = RequestMethod.GET, path = "/deletar")
	public ResponseEntity<DefaultResponse> deletarUsuario(@RequestParam("login") String login) {
		DefaultResponse response = null;
		try {
			Usuario usuario = usuarioService.buscarUsuarioPorLogin(login);
			usuarioService.deletar(usuario);
			response = new  DefaultResponse(MensagensRetorno.RETORNO_SUCESSO.getCodigo(), MensagensRetorno.RETORNO_SUCESSO.getMensagem());
		} catch (ApplicationException e) {
			response = new  DefaultResponse(e.getErrorCode(),e.getErrorMessage());
		}
		return new ResponseEntity<DefaultResponse>(response, HttpStatus.OK);
		
	}
	
	@ApiOperation(
			value="Buscar um usuário", 
			response=ResponseEntity.class, 
			notes="Essa operação realiza a busca de um usuário na base de dados.")
	@ApiResponses(value= {
			@ApiResponse(
					code=200, 
					message="Retorna um ResponseEntity com uma mensagem de sucesso",
					response=ResponseEntity.class
					),
			@ApiResponse(
					code=500, 
					message="Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception",
					response=ResponseEntity.class
					)
	})
	@RequestMapping(method = RequestMethod.GET, path = "/buscar")
	public ResponseEntity<DefaultResponse> buscarUsuario(@RequestParam("login") String login) {
		UsuarioResponse response = null;
		try {
			Usuario usuario = usuarioService.buscarUsuarioPorLogin(login);
			usuarioService.deletar(usuario);
			response = UsuarioMapper.parserUsuarioForUsuarioResponse(usuario);
		} catch (ApplicationException e) {
			response = new  UsuarioResponse(e.getErrorCode(),e.getErrorMessage());
		}
		return new ResponseEntity<DefaultResponse>(response, HttpStatus.OK);
		
	}
	
	@ApiOperation(
			value="Editar um usuário", 
			response=ResponseEntity.class, 
			notes="Essa operação realiza a edição de um usuário na base de dados.")
	@ApiResponses(value= {
			@ApiResponse(
					code=200, 
					message="Retorna um ResponseEntity com uma mensagem de sucesso",
					response=ResponseEntity.class
					),
			@ApiResponse(
					code=500, 
					message="Caso tenhamos algum erro vamos retornar um ResponseEntity com a Exception",
					response=ResponseEntity.class
					)
	})
	@RequestMapping(method = RequestMethod.POST, path = "/editar")
	public ResponseEntity<DefaultResponse> alterar(@RequestBody UsuarioRequest usuarioDTO) {
		UsuarioResponse response = null;
		try {
			Usuario usuario = usuarioService.buscarUsuarioPorLogin(usuarioDTO.getLogin());
			usuarioService.alterar(usuario);
			response = UsuarioMapper.parserUsuarioForUsuarioResponse(usuario);
		} catch (ApplicationException e) {
			response = new  UsuarioResponse(e.getErrorCode(),e.getErrorMessage());
		}
		return new ResponseEntity<DefaultResponse>(response, HttpStatus.OK);
		
	}
	
}
