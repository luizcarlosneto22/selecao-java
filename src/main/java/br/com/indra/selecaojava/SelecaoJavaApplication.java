package br.com.indra.selecaojava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import br.com.indra.selecaojava.config.FileStorageProperties;

@SpringBootApplication
@EnableConfigurationProperties({
    FileStorageProperties.class
})
public class SelecaoJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SelecaoJavaApplication.class, args);
	}

}
