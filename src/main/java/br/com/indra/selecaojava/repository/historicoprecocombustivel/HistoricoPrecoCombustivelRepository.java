package br.com.indra.selecaojava.repository.historicoprecocombustivel;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository que realiza a persistência dos dados referentes a classe histórico preço combustivel.
 * @author luiz
 *
 */
import br.com.indra.selecaojava.domain.historicoprecocombustivel.HistoricoPrecoCombustivel;

/**
 * Repository que realiza a persistência dos dados referentes a classe histórico preço combustível.
 * @author luiz
 *
 */
public interface HistoricoPrecoCombustivelRepository extends JpaRepository<HistoricoPrecoCombustivel, Long> {
	
	/**
	 * Método responsável por retornar a media de do valor das compras de combustivel baseado em um município.
	 * @param nomeMunicipio
	 * @return
	 */
	@Query(value = "SELECT NVL(SUM(H.VALORCOMPRA)/ COUNT(H.*),0) " + 
			" FROM T_HISTORICOPRECOCOMBUSTIVEL H " + 
			" INNER JOIN T_REVENDEDOR R ON R.IDREVENDEDOR = H.IDREVENDEDOR " +
			" WHERE  R.NOMEMUNICIPIO = :nomeMunicipio ", nativeQuery=true)
	BigDecimal recuperarValorMedioCompraPorMunicipio(@Param("nomeMunicipio") String nomeMunicipio);
	
	/**
	 * Método responsável por retornar a media de vendas de combustivel baseado em um município.
	 * @param nomeMunicipio
	 * @return
	 */
	@Query(value = "SELECT  NVL(SUM(H.VALORVENDA)/ COUNT(H.*),0) " + 
			" FROM T_HISTORICOPRECOCOMBUSTIVEL H " + 
			" INNER JOIN T_REVENDEDOR R ON R.IDREVENDEDOR = H.IDREVENDEDOR " +
			" WHERE R.NOMEMUNICIPIO = :nomeMunicipio ", nativeQuery=true)
	BigDecimal recuperarValorMedioVendaPorMunicipio(@Param("nomeMunicipio") String nomeMunicipio);
	

	/**
	 * Método responsável por retornar a media de compras de combustivel baseado em uma bandeira.
	 * @param nomeBandeira
	 * @return
	 */
	@Query(value = "SELECT NVL(SUM(H.VALORCOMPRA) / COUNT(H.*),0) " + 
			" FROM T_HISTORICOPRECOCOMBUSTIVEL H " + 
			" INNER JOIN T_REVENDEDOR R ON R.IDREVENDEDOR = H.IDREVENDEDOR " +
			" WHERE  H.NOMEBANDEIRA = :nomeBandeira ", nativeQuery=true)
	BigDecimal recuperarValorMedioCompraPorBandeira(@Param("nomeBandeira") String nomeBandeira);
	

	/**
	 * Método responsável por retornar a media de vendas de combustivel baseado em uma bandeira.
	 * @param nomeBandeira
	 * @return
	 */
	@Query(value = "SELECT NVL(SUM(H.VALORVENDA)/ COUNT(H.*),0) " + 
			" FROM T_HISTORICOPRECOCOMBUSTIVEL H " + 
			" INNER JOIN T_REVENDEDOR R ON R.IDREVENDEDOR = H.IDREVENDEDOR " +
			" WHERE  H.NOMEBANDEIRA = :nomeBandeira ", nativeQuery=true)
	BigDecimal recuperarValorMedioVendaPorBandeira(@Param("nomeBandeira") String nomeBandeira);
	
	/**
	 * Retorna todos os históricos pertencentes a um revendedor.
	 * @return
	 */
	@Query(value = "SELECT * " + "	FROM T_HISTORICOPRECOCOMBUSTIVEL H  "
			+ "	INNER JOIN T_REVENDEDOR R ON R.IDREVENDEDOR = H.IDREVENDEDOR" , nativeQuery = true)
	List<HistoricoPrecoCombustivel> buscarListaHistoricoPrecoCombustivel();
	
}
