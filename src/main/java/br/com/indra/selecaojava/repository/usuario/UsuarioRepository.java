package br.com.indra.selecaojava.repository.usuario;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.indra.selecaojava.domain.usuario.Usuario;

/**
 * Repository que realiza a persistência dos dados referentes a classe usuário.
 * @author luiz
 *
 */

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>{

	Usuario findByLogin(String login);
}
