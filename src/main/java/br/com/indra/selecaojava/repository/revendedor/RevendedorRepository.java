package br.com.indra.selecaojava.repository.revendedor;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.indra.selecaojava.domain.revendedor.Revendedor;
/**
 * Repository que realiza a persistência dos dados referentes a classe revendedor.
 * @author luiz
 *
 */
@Repository
public interface RevendedorRepository extends JpaRepository<Revendedor, Long> {

	Revendedor findByNomeRevendedor(String NomeRevendedor);

	Revendedor findById(long id);
	
	/**
	 * Método que retorna as informações de media do valor de vendas por município. 
	 * @param nomeMunicipio
	 * @return
	 */
	@Query(value = "SELECT NVL(SUM(H.VALORVENDA) / COUNT(H.*),0) " + "	FROM T_HISTORICOPRECOCOMBUSTIVEL H "
			+ "	INNER JOIN T_REVENDEDOR R ON R.IDREVENDEDOR = H.IDREVENDEDOR"
			+ "	WHERE R.NOMEMUNICIPIO  = :nomeMunicipio", nativeQuery = true)
	BigDecimal recuperarValorMedioPorMunicipio(@Param("nomeMunicipio") String nomeMunicipio);
	
	
	/**
	 * Método que retorna as informações de media do valor de compras por município. 
	 * @param nomeMunicipio
	 * @return
	 */
	@Query(value = "SELECT NVL(SUM(H.VALORCOMPRA)/ COUNT(H.*),0) " + "	FROM T_HISTORICOPRECOCOMBUSTIVEL H "
			+ "	JOIN T_REVENDEDOR R ON R.IDREVENDEDOR = H.IDREVENDEDOR"
			+ "	WHERE R.NOMEMUNICIPIO  = :nomeMunicipio  AND R.OPERACAO = C", nativeQuery = true)
	BigDecimal recuperarValorMedioCompraPorMunicipio(@Param("nomeMunicipio") String municipio);

	
	/**
	 * Método que retorna as informações de um revendedor que encontra-se na base de dados.
	 * @param nomeRevendedor
	 * @param regiao
	 * @param estado
	 * @param nomeMunicipio
	 * @return
	 */
	@Query(value = "SELECT * " + "	FROM T_REVENDEDOR R  "
			+ "	WHERE R.NOMEREVENDEDOR  = :nomeRevendedor  AND R.REGIAO = :regiao "
			+ "  AND R.ESTADO = :estado AND R.NOMEMUNICIPIO = :nomeMunicipio ", nativeQuery = true)
	Revendedor buscarRevendedorPorNomeEstado(@Param("nomeRevendedor") String nomeRevendedor,
			@Param("regiao") String regiao, @Param("estado") String estado,
			@Param("nomeMunicipio") String nomeMunicipio);
	
	/**
	 * Método que retorna as informações de um revendedor que encontra-se na base de dados.
	 * @param nomeMunicipio
	 * @return
	 */
	@Query(value = "SELECT * " + "	FROM T_REVENDEDOR R  "
			+ "	INNER JOIN T_HISTORICOPRECOCOMBUSTIVEL H ON R.IDREVENDEDOR = H.IDREVENDEDOR" , nativeQuery = true)
	List<Revendedor> buscarListaRevendedor();
}
