package br.com.indra.selecaojava.service.historicoprecocombustivel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.indra.selecaojava.domain.historicoprecocombustivel.HistoricoPrecoCombustivel;
import br.com.indra.selecaojava.domain.revendedor.Revendedor;
import br.com.indra.selecaojava.repository.historicoprecocombustivel.HistoricoPrecoCombustivelRepository;
import br.com.indra.selecaojava.repository.revendedor.RevendedorRepository;

/**
 * Serviço referente a classe histórico preço combustivel.
 * @author luiz
 *
 */

@Service
public class HistoricoPrecoCombustivelService {
	
	@Autowired
	private HistoricoPrecoCombustivelRepository historicoPrecoCombustivelRepository;
	
	@Autowired
	RevendedorRepository revendedorRepository;
	
	/**
	 * Metodo Responsável por salvar um histórico de preco de combustível.
	 * @param historicoPrecoCombustivel
	 * @param revendedor
	 */
	public void salvar(HistoricoPrecoCombustivel historicoPrecoCombustivel, Revendedor revendedor) {
		
		Revendedor revendaExistente = revendedorRepository.findById(Long.parseLong(revendedor.getCodigoRevendedor()));
		if (revendaExistente == null) {
			revendedor.setHistoricoPreco(new ArrayList<HistoricoPrecoCombustivel>());
			historicoPrecoCombustivelRepository.save(historicoPrecoCombustivel);
			revendedor.getHistoricoPreco().add(historicoPrecoCombustivel);
			revendedorRepository.save(revendedor);

		} else {
			historicoPrecoCombustivelRepository.save(historicoPrecoCombustivel);
			revendaExistente.getHistoricoPreco().add(historicoPrecoCombustivel);
			revendedorRepository.save(revendaExistente);
		}
	}
	
	
	/**
	 * Método responsável por realizar a soma de todas as compras de gasolina realizada em um determinado município.
	 * @param nomeMunicipio
	 * @return
	 */
	
	public BigDecimal mediaCompraCompustivelPorMunicipio(String nomeMunicipio) {
		return historicoPrecoCombustivelRepository.recuperarValorMedioCompraPorMunicipio(nomeMunicipio).setScale(2, RoundingMode.HALF_UP);
	}
	
	/**
	 * Método responsável por realizar a soma de todas as vendas de gasolina realizada em um determinado município.
	 * @param nomeMunicipio
	 * @return
	 */
	public BigDecimal mediaVendaCompustivelPorMunicipio(String nomeMunicipio) {
		return historicoPrecoCombustivelRepository.recuperarValorMedioVendaPorMunicipio(nomeMunicipio).setScale(2, RoundingMode.HALF_UP);
	}
	
	/**
	 * Método responsável por realizar a soma de todas as comprar de gasolina realizadas por uma bandeira.
	 * @param nomeBandeira
	 * @return
	 */
	
	public BigDecimal mediaCompraCompustivelPorBandeira(String nomeBandeira) {
		return historicoPrecoCombustivelRepository.recuperarValorMedioCompraPorBandeira(nomeBandeira).setScale(2, RoundingMode.HALF_UP);
	}
	
	/**
	 * Método responsável por realizar a soma de todas as vendas de gasolina realizadas por uma bandeira
	 * @param nomeBandeira
	 * @return
	 */
	public BigDecimal mediaVendaCompustivelPorBandeira(String nomeBandeira) {
		return historicoPrecoCombustivelRepository.recuperarValorMedioVendaPorBandeira(nomeBandeira).setScale(2, RoundingMode.HALF_UP);
	}
	
	/**
	 * Método responsável por retornar todas as informações importadas por data de coleta
	 * @param nomeBandeira
	 * @return
	 */
	public List<HistoricoPrecoCombustivel> buscarDadosAgrupadosPorDataColeta() {
		List<HistoricoPrecoCombustivel> lista = historicoPrecoCombustivelRepository.buscarListaHistoricoPrecoCombustivel();
		lista.sort((a,b) -> a.getDataColeta().compareTo(b.getDataColeta()));
		return lista;
	}
	
}
