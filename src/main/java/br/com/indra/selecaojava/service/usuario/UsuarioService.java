package br.com.indra.selecaojava.service.usuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.indra.selecaojava.domain.enums.MensagensRetorno;
import br.com.indra.selecaojava.domain.usuario.Usuario;
import br.com.indra.selecaojava.exception.ApplicationException;
import br.com.indra.selecaojava.repository.usuario.UsuarioRepository;

/**
 * Serviço referente a classe de usuário.
 * @author luiz
 *
 */

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	/**
	 * Metodo responśvel por salvar um usuário
	 * @param usuario
	 */
	public void salvar(Usuario usuario) {
		usuarioRepository.save(usuario);
		
	}

	/**
	 * Método responsável por deletar um usuário na base de dados
	 * @param usuario
	 */
	public void deletar(Usuario usuario) {
		usuarioRepository.delete(usuario);
		
	}

	/**
	 * Método responsável por alterar um usuário na base de dados
	 * @param usuario
	 */
	public void alterar(Usuario usuario) {
		usuarioRepository.saveAndFlush(usuario);
		
	}
	
	/**
	 * Método responsável por buscar um usuario pelo seu login
	 * @param login
	 * @return
	 * @throws ApplicationException
	 */
	public Usuario buscarUsuarioPorLogin(String login) throws ApplicationException {
		Usuario usuario  = usuarioRepository.findByLogin(login);
	    if (usuario == null) {
	    	 throw new ApplicationException(MensagensRetorno.USUARIO_NAO_ENCONTRATO.getCodigo(), MensagensRetorno.USUARIO_NAO_ENCONTRATO.getMensagem());
	    }
	    return usuario;
	}
	
}
