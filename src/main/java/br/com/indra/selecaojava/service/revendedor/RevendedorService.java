package br.com.indra.selecaojava.service.revendedor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.indra.selecaojava.domain.enums.MensagensRetorno;
import br.com.indra.selecaojava.domain.revendedor.Revendedor;
import br.com.indra.selecaojava.exception.ApplicationException;
import br.com.indra.selecaojava.repository.revendedor.RevendedorRepository;

/**
 * Serviço referente a classe de revendedor.
 * @author luiz
 *
 */

@Service
public class RevendedorService {

	@Autowired
	private RevendedorRepository revendedorRepository;
	
	/**
	 * Método responsável por retornar um revendedor baseado no nome da revenda
	 * @param nomeRevendedor
	 * @return
	 * @throws ApplicationException
	 */
	public Revendedor buscarRevendaPorNomeRevenda(String nomeRevendedor) throws ApplicationException {
		Revendedor revendedor = revendedorRepository.findByNomeRevendedor(nomeRevendedor);
		if (revendedor == null) {
	    	 throw new ApplicationException(MensagensRetorno.USUARIO_NAO_ENCONTRATO.getCodigo(), MensagensRetorno.USUARIO_NAO_ENCONTRATO.getMensagem());
		}
		return revendedor;
	}
	
	/**
	 * Método responsável por retornar um revendedor baseado no seu código
	 * @param codigo
	 * @return
	 * @throws ApplicationException
	 */
	public Revendedor buscarRevendaPorCodigo(long codigo) throws ApplicationException {
		Revendedor revendedor = revendedorRepository.findById(codigo);
		if (revendedor == null) {
	    	 throw new ApplicationException(MensagensRetorno.USUARIO_NAO_ENCONTRATO.getCodigo(), MensagensRetorno.USUARIO_NAO_ENCONTRATO.getMensagem());
		}
		return revendedor;
	}
	
	/**
	 * Método responsável por retornar a media de preco baseado no nome do municipio
	 * @param nomeMunicipio
	 * @return
	 */
	public BigDecimal buscarMediaDePrecoPorMunicipio(String nomeMunicipio) {
		return revendedorRepository.recuperarValorMedioPorMunicipio(nomeMunicipio).setScale(2, RoundingMode.HALF_UP);
	}
	
	/**
	 * Método responsável porretornar todos os dados agrupador por regiao
	 * @return
	 */
	public List<Revendedor> buscarDadosAgrupadosPorRegiao() {
		List<Revendedor> lista = revendedorRepository.buscarListaRevendedor();
		lista.sort((a,b) -> a.getRegiao().compareTo(b.getRegiao()));
		return lista;
	}
	
	/**
	 * Método responsável por buscar todos os dados agrupador por nome do revendedor
	 * @return
	 */
	public List<Revendedor> buscarDadosAgrupadosPorRevendedor() {
		List<Revendedor> lista = revendedorRepository.buscarListaRevendedor();
		lista.sort((a,b) -> a.getNomeRevendedor().compareTo(b.getNomeRevendedor()));
		return lista;
	}
	
	
}
