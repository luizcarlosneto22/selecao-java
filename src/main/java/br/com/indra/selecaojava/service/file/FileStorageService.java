package br.com.indra.selecaojava.service.file;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.indra.selecaojava.config.FileStorageProperties;
import br.com.indra.selecaojava.domain.historicoprecocombustivel.HistoricoPrecoCombustivel;
import br.com.indra.selecaojava.domain.revendedor.Revendedor;
import br.com.indra.selecaojava.exception.FileStorageException;
import br.com.indra.selecaojava.repository.revendedor.RevendedorRepository;
import br.com.indra.selecaojava.util.ArquivoParser;
import br.com.indra.selecaojava.util.LinhaAquivoImportacao;
import br.com.indra.selecaojava.util.Util;

/**
 * Serviço que contém as regras para importação do arquvi csv.
 * @author luiz
 *
 */

@Service
public class FileStorageService {

    private final Path fileStorageLocation;
    
    @Autowired
    private RevendedorRepository revendedorRepository;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }
    
    /**
     * Método responsável por importar o arquivo .csv e cadastrar os dados pertencentes a ele.
     * @param file
     * @throws IOException
     */
    public void importarArquivoCadastrando(MultipartFile file) throws IOException {
    		List<LinhaAquivoImportacao> listaLinhasImportadas = Util.lerArquivo(file.getInputStream());
    		listaLinhasImportadas.forEach(linha->{
    			Revendedor revendedor = revendedorRepository.buscarRevendedorPorNomeEstado(linha.getRevendedor(), linha.getRegiao(), linha.getEstado(), linha.getMunicipio());
    			HistoricoPrecoCombustivel historico = new HistoricoPrecoCombustivel();
    			if (revendedor == null) {
    				revendedor = ArquivoParser.converteLinhaToRevendedo(linha);
    				revendedor.setHistoricoPreco(new ArrayList<>());
    				historico  = ArquivoParser.converteLinhaToHistorico(linha);
    				revendedor.getHistoricoPreco().add(historico);
    				historico.setRevendedor(revendedor);
    				revendedorRepository.save(revendedor);
    				
    			} else {
    				historico  = ArquivoParser.converteLinhaToHistorico(linha);
    				revendedor.getHistoricoPreco().add(historico);
    				historico.setRevendedor(revendedor);
    				revendedorRepository.save(revendedor);
    			}
    			
    		});
    }
}