package br.com.indra.selecaojava.domain.enums;

/**
 *  Enum que contém as mensagens de retorno das operações realizadas.
 *  @author luiz
 *
 */
public enum MensagensRetorno {

	RETORNO_SUCESSO("00", "Operação Realizada com sucesso!"),
	USUARIO_NAO_ENCONTRATO("01", "Usuário não encontrato"),
	CAMPO_NAO_INFORMADO("02", "Valor não informado"),
	ERRO_INTERNO("99", "Erro interno");
	
	
	private String mensagem;
	private String codigo;
	
	
	private MensagensRetorno(String mensagem, String codigo) {
		this.mensagem = mensagem;
		this.codigo = codigo;
	}


	public String getMensagem() {
		return mensagem;
	}


	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	
}