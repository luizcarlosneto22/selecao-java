package br.com.indra.selecaojava.domain.enums;

/**
 * Enum que contém as informações do tipo de combustível que foi cadastrado.
 * @author luiz
 *
 */
public enum TipoCombustivel {
	GASOLINA(1, "GASOLINA"), 
	ETANOL(2, "ETANOL"), 	
	DIESEL(3, "DIESEL"),
	GNV(4,"GNV");

	private TipoCombustivel(int cod, String descricao) {
		this.cod = cod;
		this.descricao = descricao;
	}

	private int cod;
	private String descricao;

	public int getCod() {
		return cod;
	}

	public void setCod(int cod) {
		this.cod = cod;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public static TipoCombustivel recuperaValorPorDescricao(String descricao) {		
		for (TipoCombustivel combustivel : TipoCombustivel.values()) {
			if(combustivel.getDescricao().equals(descricao)) {
				return combustivel;
			}
		}
		return null;
	}
	
}

