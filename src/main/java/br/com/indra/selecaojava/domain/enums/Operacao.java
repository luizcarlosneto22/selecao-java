package br.com.indra.selecaojava.domain.enums;

/**
 * Enum que contém as operações que são realizadas, como por exemplo,  compra e venda de combustível.
 * @author luiz
 *
 */
public enum Operacao {

	COMPRA("C", "COMPRA"), 
	VENDA("V", "VENDA");

	private Operacao(String tipo, String descricao) {
		this.tipo = tipo;
		this.descricao = descricao;
	}

	private String tipo;
	private String descricao;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
