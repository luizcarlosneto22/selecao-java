package br.com.indra.selecaojava.domain.historicoprecocombustivel;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.indra.selecaojava.domain.enums.Operacao;
import br.com.indra.selecaojava.domain.enums.TipoCombustivel;
import br.com.indra.selecaojava.domain.enums.UnidadeDeMedida;
import br.com.indra.selecaojava.domain.revendedor.Revendedor;
import lombok.Data;

/**
 * Classe que contém as informações referentes ao histórico de preço de combustível.
 * @author luiz
 *
 */

@Entity
@Table(name = "T_HISTORICOPRECOCOMBUSTIVEL")
@SequenceGenerator(name="seq", sequenceName="S_HISTORICOPRECOCOMBUSTIVEL", allocationSize=1)
@Data
public class HistoricoPrecoCombustivel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
	@Column(name = "IDHISTORICOPRECOCOMBUSTIVEL")
	private Long id;

	@Column(name = "CODIGOPRODUTO")
	private String codigoProduto;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "PRODUTO")
	private TipoCombustivel produto;

	@Temporal(TemporalType.DATE)
	@Column(name = "DATACOLETA")
	private Date dataColeta;

	@Column(name = "VALORCOMPRA")
	private BigDecimal valorCompra;

	@Column(name = "VALORVENDA")
	private BigDecimal valorVenda;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "UNIDADEMEDIDA")
	private UnidadeDeMedida unidadeMedida;

	@Enumerated(EnumType.STRING)
	@Column(name = "OPERACAO")
	private Operacao tipoOperacao;

	@Column(name = "NOMEBANDEIRA")
	private String nomeBandeira;

	@ManyToOne
	@JoinColumn(name = "IDREVENDEDOR")
	private Revendedor revendedor;

	public HistoricoPrecoCombustivel(String codigoProduto, TipoCombustivel produto, Date dataColeta,
			 BigDecimal valorCompra, BigDecimal valorVenda, UnidadeDeMedida unidadeMedida,
			Operacao tipoOperacao, String nomeBandeira) {
		this.codigoProduto = codigoProduto;
		this.produto = produto;
		this.dataColeta = dataColeta;
		this.valorCompra = valorCompra;
		this.valorVenda = valorVenda;
		this.unidadeMedida = unidadeMedida;
		this.tipoOperacao = tipoOperacao;
		this.nomeBandeira = nomeBandeira;
	}
	
	public HistoricoPrecoCombustivel() {};
	
	
}
