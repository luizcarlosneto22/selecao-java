package br.com.indra.selecaojava.domain.revendedor;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.indra.selecaojava.domain.historicoprecocombustivel.HistoricoPrecoCombustivel;
import lombok.Data;

/**
 * Classe que contém as informações de um revendedor.
 * @author luiz
 *
 */
@Entity
@Table(name = "T_REVENDEDOR")
@SequenceGenerator(name="seq", sequenceName="S_REVENDEDOR", allocationSize=1)
@Data
public class Revendedor {
	private static final long serialVersionUID = -4688473406909379923L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
	@Column(name = "IDREVENDEDOR")
	private Long id;
	
	@Column(name = "REGIAO")
	private String regiao;

	@Column(name = "ESTADO")
	private String estado;

	@Column(name = "NOMEMUNICIPIO")
	private String nomeMunicipio;

	@Column(name = "NOMEREVENDEDOR")
	private String nomeRevendedor;

	@OneToMany(mappedBy = "revendedor", targetEntity = HistoricoPrecoCombustivel.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonBackReference
	private List<HistoricoPrecoCombustivel> historicoPreco;
	
	@Transient
	private String codigoRevendedor;
	
	public Revendedor(String regiao, String estado, String nomeMunicipio, String nomeRevendedor) {
		this.regiao = regiao;
		this.estado = estado;
		this.nomeMunicipio = nomeMunicipio;
		this.nomeRevendedor = nomeRevendedor;
	}

	public Revendedor() {};
	
	
	
	
}
