package br.com.indra.selecaojava.domain.enums;
/**
 * Enum que contém o tipo de unidade de medida dos combustíveis.
 * @author luiz
 *
 */
public enum UnidadeDeMedida {
	
	LITRO("L", "LITRO");

	private UnidadeDeMedida(String cod, String descricao) {
		this.cod = cod;
		this.descricao = descricao;
	}

	private String cod;
	private String descricao;

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


}
